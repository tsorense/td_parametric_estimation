% MAIN - perform factor analysis of the contours in the files
% contourdata_<dataset>.mat
% 
% Tanner Sorensen
% Signal Analysis and Interpretation Laboratory
% Feb. 16, 2017

% variability dataset
get_Ugfa(configStruct,'var')
