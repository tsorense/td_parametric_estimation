function U_jaw = get_Ujaw(contourdata,folder)
% GET_UJAW - obtain the jaw factor
% 
% INPUT:
%  Variable name: contourdata 
%  Size: 1x1
%  Class: struct
%  Description: Struct with fields for each subject (field name is subject 
%    ID, e.g., 'at1_rep'). The fields are structs with the following 
%    fields.
%  Fields: 
%  - X: X-coordinates of tissue-air boundaries in columns and time-samples 
%      in rows
%  - Y: Y-coordinates of tissue-air boundaries in columns and time-samples 
%      in rows
%  - File: file ID for each time-sample, note that this indexes the cell 
%      array of string file names in fl
%  - fl: cell array of string file names indexed by the entries of File
%  - SectionsID: array of numeric IDs for X- and Y-coordinates in the 
%      columns of the variables in fields X, Y; the correspondences are as 
%      follows: 01 Epiglottis; 02 Tongue; 03 Incisor; 04 Lower Lip; 05 Jaw;
%      06 Trachea; 07 Pharynx; 08 Upper Bound; 09 Left Bound; 10 Low Bound;
%      11 Palate; 12 Velum; 13 Nasal Cavity; 14 Nose; 15 Upper Lip
%  - Frames: frame number; 1 is first segmented video frame
%  - VideoFrames: frame number; 1 is first frame of avi video file
%  
%  Variable name: folder
%  Size: arbitrary
%  Class: char
%  Description: determines which participant/scan to analyze.
% 
% FUNCTION OUTPUT:
%  Variable name: U_jaw
%  Size: 400x1
%  Class: double
%  Description: entries correspond to coordinates of the factors on the 
%    (X,Y)-plane. 
% 
% SAVED OUTPUT: 
%  none
% 
% Tanner Sorensen
% Signal Analysis and Interpretation Laboratory
% Apr. 14, 2017

% dataset
contourdata=contourdata;
D=[contourdata.(folder).X,contourdata.(folder).Y];
[Dnorm,meandata] = zscore(D);

%% Principal components analysis
% subset data to jaw
SectionsID=contourdata.(folder).SectionsID;
SecID2=[SectionsID,SectionsID];
vtsection=5;                                    % jaw
Dnorm_zero = Dnorm;
Dnorm_zero(:,~ismember(SecID2,vtsection))=0;

% principal components analysis of the jaw
[U,~,~,~,varpercent] = pca(Dnorm_zero);
U_jawraw = U;

% close all;
% 
% figure(1);
% 
% for i=1:6
%     
%     subplot(2,3,i);
%     
%     DD = Dnorm_zero*U(:,i)*pinv(U(:,i));
% 
%     plot_from_xy(meandata+2*std(DD),SectionsID(1,:),'b'); hold on;
%     plot_from_xy(meandata-2*std(DD),SectionsID(1,:),'r'); hold on;
%     
%     plot_from_xy(meandata,SectionsID(1,:),'k');
%     
%     title(['Component ',num2str(i),', Total Variance Explained: ',num2str(varpercent(i))]);
%     
%     axis equal; axis off;
%     
% end;

%% Guided factor analysis
% subset data to tongue-jaw region
vtsection = [2 4 5];                              % jaw, tongue, lip region
Dnorm_zero = Dnorm;
Dnorm_zero(:,~ismember(SecID2,vtsection))=0;  % zero out contours not in 
                                              % tongue-jaw region

% obtain data covariance matrix
n = size(D,1);
R = Dnorm_zero'*Dnorm_zero/n; % covariance matrix

% obtain jaw factor
t1 = U_jawraw(:,1); % first jaw PC
v = t1'*R*t1;       % variance of the firt jaw PC
h1 = t1/sqrt(v);    % first jaw PC with unit variance 
                    % (i.e., var(Dnorm*h1) approximately equals 1)
f1 = (h1'*R)';      % scale the first jaw PC by data covariance
U_jaw = f1;         % jaw factor

% figure(2)
% 
% subplot(1,2,2)
% 
% DD = D*f1*pinv(f1);
% 
% plot_from_xy(meandata+2*std(DD),SectionsID(1,:),'b'); hold on;
% plot_from_xy(meandata-2*std(DD),SectionsID(1,:),'r'); hold on;
% 
% plot_from_xy(meandata,SectionsID(1,:),'k');
% axis equal; axis off;
% 
% subplot(1,2,1)
% 
% DD = Dnorm_zero*U(:,1)*pinv(U(:,1));
% 
% plot_from_xy(meandata+2*std(DD),SectionsID(1,:),'b'); hold on;
% plot_from_xy(meandata-2*std(DD),SectionsID(1,:),'r'); hold on;
% 
% plot_from_xy(meandata,SectionsID(1,:),'k');
% axis equal; axis off;