% WRAP_MAKE_CONTOUR_DATA - makes a MAT file containing all segmentation 
% results in configStruct.datPath and saving the MAT file at the location 
% configStruct.outPath.
% 
% Tanner Sorensen
% Signal Analysis and Interpretation Laboratory
% Feb. 14, 2017

% Each call to make_contour_data below generates the file 
% contourdata_<dataset>.mat

% variability dataset
make_contour_data(configStruct,'var')