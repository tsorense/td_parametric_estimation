% This file calls the functions make_hpc_csv_rep, make_hpc_csv_nsf, and 
% make_hpc_csv_var to make the CSV files used for segmenting the rtMRI 
% videos on the cluster.
% 
% Before running this script, you must have made manual annotations of the
% timestamps in the avi files. 
% 
% Tanner Sorensen
% Signal Analysis and Interpretation Laboratory
% Feb. 14, 2017

% A. repeatability data-set
subj_id = {'at1_rep','cl2_rep','cv2_rep','eh3_rep',...
    'sg2_rep','tj1_rep','vl1_rep','yb1_rep'};
path_in = '/home/tsorense/spring2017/manual_annotations/timestamps_rep.xlsx';
path_out = '/home/tsorense/spring2017/scripts/segmentation/wrappers_%s';

for i=1:length(subj_id)
    make_hpc_csv_rep(subj_id{i}, configStruct, path_in, sprintf(path_out,subj_id{i}))
end

% B. NSF data-set
subj_id = {'js1_nsf','pg2_nsf','pp1_nsf','sn1_nsf'};
path_in = '/home/tsorense/spring2017/manual_annotations/timestamps_nsf.xlsx';
path_out = '/home/tsorense/spring2017/scripts/segmentation/wrappers_%s';

for i=1:length(subj_id)
    make_hpc_csv_nsf(subj_id{i}, configStruct, path_in, sprintf(path_out,subj_id{i}))
end

% C. variability data-set
subj_id = {'ac2_var','ak3_var','am1_var','bp3_var','ch2_var','cs2a_var','cs2b_var',...
    'eb1_var','ec1_var','er1_var','jr2_var','jt2_var','mf3_var','mm3_var'};
path_in = '/home/tsorense/spring2017/manual_annotations/timestamps_var.xlsx';
path_out = '/home/tsorense/spring2017/scripts/segmentation/wrappers_%s';

for i=1:length(subj_id)
    make_hpc_csv_var(subj_id{i}, configStruct, path_in, sprintf(path_out,subj_id{i}))
end

% D. repeatability 2 data-set
subj_id = {'at1_rep2','cl2_rep2','cv2_rep2','eh3_rep2',...
    'sg2_rep2','tj1_rep2','vl1_rep2','yb1_rep2'};
path_in = '/home/tsorense/spring2017/manual_annotations/timestamps_rep2.xlsx';
path_out = '/home/tsorense/spring2017/scripts/segmentation/wrappers_%s';

for i=1:length(subj_id)
    make_hpc_csv_rep(subj_id{i}, configStruct, path_in, sprintf(path_out,subj_id{i}))
end
