
% Get directories
dirs = dir(fullfile(cd,'wrappers*'));
dirflag = [dirs.isdir];
dirs = dirs(dirflag);

% Make cluster files
for i=1:length(dirs)
    disp(dirs(i).name)
    cd(dirs(i).name)
    system('rm -r cluster');
    wrap_make_batch
    cd ..
end