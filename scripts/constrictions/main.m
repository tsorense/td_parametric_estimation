% MAIN - discover constriction locations from the data, measure
% constriction degrees, and plot constriction degrees.
% 
% see also getTV, plotTV
% 
% Tanner Sorensen
% Signal Analysis and Interpretation Laboratory
% Feb. 14, 2017

% variability dataset
% measure task variables
getTV(configStruct,'var')

% plot task variable figure
plotTV(configStruct,'var')