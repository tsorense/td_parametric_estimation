function configStruct = config
% CONFIG - set constants and parameters of the analysis
% 
% INPUT
%  none
% 
% FUNCTION OUTPUT:
%  Variable name: configStruct
%  Size: 1x1
%  Class: struct
%  Description: Fields correspond to constants and hyperparameters. 
%  Fields: 
%  - outPath: (string) path for saving MATLAB output
%  - aviPath: (string) path to the AVI files
%  - graphicsPath: (string) path to MATALB graphical output
%  - trackPath: (string) path to segmentation results
%  - manualAnnotationsPath: (string) path to manual annotations
%  - timestamps_file_name_<dataset>: (string) file name with path of 
%      timestamps file name for each data-set <dataset> of the analysis
%  - folders_<dataset>: (cell array) string folder names which belong to 
%      each data-set <dataset> of the analysis
%  - tasks: (cell array) string identifiers for different tasks
%  - FOV: (double) size of field of view in mm^2
%  - Npix: (double) number of pixels per row/column in the imaging plane
%  - framespersec_<dataset>: (double) frame rate of reconstructed real-time
%      magnetic resonance imaging videos in frames per second for each 
%      data-set <dataset> of the analysis
%  - ncl: (double array) entries are (i) the number of constriction 
%      locations at the hard and soft palate and (ii) the number of 
%      constriction locations at the hypopharynx (not including the 
%      nasopharynx).
%  - f: (double) hyperparameter which determines the percent of data used 
%      in locally weighted linear regression estimator of the jacobian; 
%      multiply f by 100 to obtain the percentage
%  - verbose: controls non-essential graphical and text output
% 
% SAVED OUTPUT: 
%  none
% 
% EXAMPLE USAGE: 
%  >> configStruct = config;
% 
% Tanner Sorensen
% Signal Analysis and Interpretation Laboratory
% Feb. 14, 2017

% paths
absPath = '/home/tsorense/spring2017/new_forward_map';
outPath = fullfile(absPath,'mat');
aviPath = fullfile(absPath,'data');
graphicsPath = fullfile(absPath,'graphics');
trackPath = fullfile(absPath,'mat','segmentation_results');

manualAnnotationsPath = fullfile(absPath,'manual_annotations');

% timestamps file name
timestamps_file_name_var = fullfile(absPath,'manual_annotations/timestamps_var.xlsx');

% array constants
folders_var = {'ac2_var','ak3_var','am1_var','bp3_var','ch2_var',...
    'cs2a_var','cs2b_var','eb1_var','ec1_var','er1_var','jr2_var',...
    'jt2_var','mf3_var','mm3_var'};

% fixed parameters
FOV = 200; % 200 mm^2 field of view 
Npix = 68; % 68^2 total pixels
%spatRes = FOV/Npix; % spatial resolution
framespersec_var = 1/(7*0.006164);
ncl = [3 1];

% free parameters
f = 0.1;

% control printed output
verbose = false;

% make the struct object
configStruct = struct('outPath',outPath,'aviPath',aviPath,...
    'graphicsPath',graphicsPath,'trackPath',trackPath,...
    'timestamps_file_name_var',timestamps_file_name_var,...
    'manualAnnotationsPath',manualAnnotationsPath,...
    'folders_var',{folders_var},...
    'FOV',FOV,'Npix',Npix,...%'spatRes',spatRes,...
    'framespersec_var',framespersec_var,...
    'f',f,'ncl',ncl,'verbose',verbose);

end