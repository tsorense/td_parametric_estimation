Task Dynamics Parametric Estimation
===================================

This folder contains files for performing parametric estimation of the forward kinematic map and the associated jacobian for the Task Dynamics model of speech production. Parametric estimation is based on articulator segmentations extracted from real-time magnetic resonance imaging videos. The scripts are essentially the same as those used in the following paper.  

Sorensen, T., Toutios, A., Goldstein, L., Narayanan, S. Characterizing vocal tract dynamics across speakers using real-time MRI. In Proceedings of Interspeech, 2016. [ISCA Award for Best Student Paper at Interspeech 2016] doi: 10.21437/Interspeech.2016-583


Contact information:  
 Tanner Sorensen  
 Signal Analysis and Interpretation Laboratory  
 <tsorense@usc.edu>



Directories
-----------

- `data` - copy MRI data-sets to this folder
- `graphics` - graphical output of MATLAB programs
- `manual_annotations` - manually annotations which serve as inputs to MATLAB programs
- `mat` - segmentation results, articulatory strategy biomarkers output by MATLAB programs
- `scripts` - MATLAB programs
  - `constrictions` - manually annotate constriction locations, automatically measure constriction degrees
  - `data_scripts` - convert segmentation file format
  - `factor_analysis` - perform guided factor analysis on segmentation results
  - `segmentation` - files used to segment the articulators in rt-MRI videos
  - `simulation` - perform parametric estimation and simulate vocal tract constrictions
  - `util` - configuration file, plotting function
  - `main.m` - extracts articulatory strategies from track files


Software versions
-----------------

Linux distribution release:

    Distributor ID:	Ubuntu
    Description:	Ubuntu 16.04.2 LTS
    Release:	16.04
    Codename:	xenial

MATLAB

    MATLAB Version: 9.1.0.441655 (R2016b)
    MATLAB License Number: 623588
    Operating System: Linux 4.4.0-72-generic #93-Ubuntu SMP Fri Mar 31 14:07:41 UTC 2017 x86_64
    Java Version: Java 1.7.0_60-b19 with Oracle Corporation Java HotSpot(TM) 64-Bit Server VM mixed mode
    MATLAB                                                Version 9.1         (R2016b)
    Simulink                                              Version 8.8         (R2016b)
    Bioinformatics Toolbox                                Version 4.7         (R2016b)
    Control System Toolbox                                Version 10.1        (R2016b)
    Curve Fitting Toolbox                                 Version 3.5.4       (R2016b)
    DSP System Toolbox                                    Version 9.3         (R2016b)
    Financial Toolbox                                     Version 5.8         (R2016b)
    Image Processing Toolbox                              Version 9.5         (R2016b)
    Instrument Control Toolbox                            Version 3.10        (R2016b)
    Optimization Toolbox                                  Version 7.5         (R2016b)
    Parallel Computing Toolbox                            Version 6.9         (R2016b)
    Signal Processing Toolbox                             Version 7.3         (R2016b)
    Simscape                                              Version 4.1         (R2016b)
    Simscape Multibody                                    Version 4.9         (R2016b)
    Simulink Control Design                               Version 4.4         (R2016b)
    Stateflow                                             Version 8.8         (R2016b)
    Statistics and Machine Learning Toolbox               Version 11.0        (R2016b)
    Symbolic Math Toolbox                                 Version 7.1         (R2016b)